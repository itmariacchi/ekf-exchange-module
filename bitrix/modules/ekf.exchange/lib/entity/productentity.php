<?php
namespace Ekf\Exchange\Entity;

/**
 * Из за специфики работы АПИ товар - это непосредственно товар плюс несколько свойств
 */
class ProductEntity
{
    private $id;

    private $iblockId;

    private $sectionId;

    private $xmlId;

    private $name;

    private $code;

    private $active;

    private $previewText;

    /** @var array[PropertyValueEntity] */
    private $propertyCollection;

    public function toState()
    {
        $state = [];

        $state['ID'] = $this->getId() ? $this->getId() : false;
        $state['IBLOCK_ID'] = $this->getIblockId() ? $this->getIblockId() : false;
        $state['IBLOCK_SECTION_ID'] = $this->getSectionId() ? $this->getSectionId() : false;
        $state['XML_ID'] = $this->getXmlId() ? $this->getXmlId() : false;
        $state['NAME'] = $this->getName() ? $this->getName() : false;
        $state['CODE'] = $this->getCode() ? $this->getCode() : false;
        $state['ACTIVE'] = $this->getActive() ? 'Y' : 'N';
        $state['PREVIEW_TEXT'] = $this->getPreviewText() ? $this->getPreviewText() : '';

        /** @var PropertyValueEntity $valueEntity */
        foreach ($this->getPropertyCollection() as $valueEntity) {
            $propCode = $valueEntity->getPropertyCode();

            if (!empty($propCode)) {
                $state['PROPERTIES'][$propCode] = $valueEntity->getValue();
            }
        }

        $state = array_filter($state);

        return $state;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getXmlId()
    {
        return $this->xmlId;
    }

    /**
     * @param mixed $xmlId
     */
    public function setXmlId($xmlId)
    {
        $this->xmlId = $xmlId;
    }

    /**
     * @return mixed
     */
    public function getIblockId()
    {
        return $this->iblockId;
    }

    /**
     * @param mixed $iblockId
     */
    public function setIblockId($iblockId)
    {
        $this->iblockId = $iblockId;
    }

    /**
     * @return mixed
     */
    public function getSectionId()
    {
        return $this->sectionId;
    }

    /**
     * @param mixed $sectionId
     */
    public function setSectionId($sectionId)
    {
        $this->sectionId = $sectionId;
    }

    /**
     * @return mixed
     */
    public function getPropertyCollection()
    {
        return $this->propertyCollection;
    }

    /**
     * @param mixed $propertyCollection
     */
    public function setPropertyCollection($propertyCollection)
    {
        $this->propertyCollection = $propertyCollection;
    }

    /**
     * @param PropertyValueEntity $propertyValue
     */
    public function addToPropertyCollection($propertyValue)
    {
        $this->propertyCollection[] = $propertyValue;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getPreviewText()
    {
        return $this->previewText;
    }

    /**
     * @param mixed $previewText
     */
    public function setPreviewText($previewText)
    {
        $this->previewText = $previewText;
    }
}