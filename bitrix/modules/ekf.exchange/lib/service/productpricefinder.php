<?php
namespace Ekf\Exchange\Service;

use InvalidArgumentException;

class ProductPriceFinder
{
    private $productsCache = [];

    /** @var Config  */
    private $config;

    public function __construct($iblockId)
    {
        if ((int)$iblockId == 0) {
            throw new InvalidArgumentException(sprintf(
                'Ид инфоблока должен быть числом: "%s"', $iblockId
            ));
        }

        $this->config = new Config();


        $this->fillPricesMap($iblockId);
    }

    /**
     * Заполняет массив данными о существующих ценах
     * @param $iblockId
     */
    private function fillPricesMap($iblockId)
    {
        $priceMap = $this->config->getPricesMap();

        $select = array('ID', 'XML_ID');

        foreach ($priceMap as $apiId => $sitePriceId) {
            $select[] = 'CATALOG_GROUP_' . $sitePriceId;
        }

        $dbElements = \CIBlockElement::GetList(
            array(),
            array('IBLOCK_ID' => $iblockId, 'CHECK_PERMISSIONS' => 'N', 'PROPERTY_EKF_IS_EKF' => 1),
            false,
            false,
            $select
        );

        $this->productsCache = [];

        while ($arElement = $dbElements->Fetch()) {
            $this->productsCache[$arElement['XML_ID']] = [];

            $prices = [];

            foreach ($priceMap as $apiId => $sitePriceId) {
                $prices[$apiId] = [
                    'id' => (int)$arElement['CATALOG_PRICE_ID_' . $sitePriceId],
                    'price' => $arElement['CATALOG_PRICE_' . $sitePriceId]
                ];
            }

            $this->productsCache[$arElement['XML_ID']] = [
                'ID' => $arElement['ID'],
                'PRICES' => $prices
            ];
        }
    }

    /**
     * @param $productXmlID
     * @return array
     */
    public function getExisting($productXmlID)
    {
        $existingProduct = $this->productsCache[$productXmlID];

        if (isset($existingProduct)) {
            return $existingProduct;
        }

        return [];
    }
}