<?php
/**
 * @author Pavel Ivanov <itmariacchi@gmail.com>
 * Date: 31.07.2018
 *
 * @var array $apiSectionsByParent
 * @var array $section
 */

$children = $apiSectionsByParent[$section['id']];

if (!is_array($children) || count($children) == 0) {
    return;
}
?>

<? foreach ($children as $section) { ?>

    <div class="section-mapper__section section-depth-<?= $section['hierarchyLevel'] ?>">

        <div class="section-mapper__collapser">
            <? if (!empty($apiSectionsByParent[$section['id']])) { ?>
                <a class="section-mapper__section-collapse"></a>
            <? } else { ?>
                &nbsp;
            <? } ?>
        </div>
        <div class="section-mapper__section-name">
            <a href="#" class="section-mapper__matcher" data-section-id="<?= $section['id'] ?>" data-section-name="<?= $section['name'] ?>">←</a>
            <?= $section['name'] ?>
        </div>
        <div class="section-mapper__clearfix"></div>

        <? require(__DIR__ . '/sections.php'); ?>

    </div>

<? } ?>