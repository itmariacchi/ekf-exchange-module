/**
 * Блок в левой панели, соотвествующий разделу АПИ, который будет выгружаться в раздел сайта
 * @constructor
 */
var SectionMatchedItem = function(siteId, apiId, apiName, collapse)
{
    this.siteId = siteId;
    this.apiId = apiId;
    this.apiName = apiName;
    this.collapse = collapse;

    this.onDelete = null;

    this.$el = null;
};

/**
 * Отрисовать блок внутри $parent
 * @param $parent
 */
SectionMatchedItem.prototype.render = function($parent)
{
    var checkedHtml = (this.collapse) ? 'checked' : '';

    var html = '<div class="section-match__matched-item">'
        + '<a class="section-match__matched-del">x</a>'
        + '<div class="section-match__matched-name">' + this.apiName
        + ' <input type="checkbox" name="ekf_exchange[sections_map][collapse][' + this.siteId + '][' + this.apiId + ']" value="y" title="Схлопывать артикула" ' + checkedHtml + '>'
        + '</div>'
        + '<input type="hidden" name="ekf_exchange[sections_map][match][' + this.siteId + '][]" value="' + this.apiId + '">'
        + '</div>';

    this.$el = $(html);

    this.initEventHandlers(this.$el);

    $parent.append(this.$el);
};

/**
 * Инициализация обработчиков на одном элементе
 * @param $el
 */
SectionMatchedItem.prototype.initEventHandlers = function($el)
{
    var self = this;

    // Удаление блока
    $el.find('.section-match__matched-del').click(function(){
        self.$el.remove();

        if (self.onDelete) {
            self.onDelete();
        }

        return false;
    });
};