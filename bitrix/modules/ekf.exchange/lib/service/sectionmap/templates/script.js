$(function(){
    /**
     * Сворачиваем-разворачиваем дерево разделов
     */
    $('.section-mapper__section-collapse').click(function(){
        $(this).closest('.section-mapper__section').toggleClass('open');
        return false;
    });

    /**
     * При клике на раздел в дереве категорий, выделим его
     */
    $('.section-mapper__section-name').click(function(){
        var container = $(this).closest('.section-mapper__sections');

        var $apiSectionWrap = $(this).parents('.section-mapper__section');

        // Если раздел уже сопоставлен
        if ($apiSectionWrap.hasClass('disabled')) {
            return false;
        }

        var parent = $(this).parent();

        container.find('.section-mapper__section').removeClass('selected');
        parent.addClass('selected');
    });

    /**
     * Стрелка влево (выбор раздела)
     */
    var matchCollection = new SectionMatchedItemCollection();

    $('.section-mapper__matcher').click(function(){
        var $apiSectionWrap = $(this).closest('.section-mapper__section');

        // Если раздел уже сопоставлен
        if ($apiSectionWrap.hasClass('disabled')) {
            return false;
        }

        var $selectedSiteSection = $('#section-mapper-site .section-mapper__section.selected');

        if ($selectedSiteSection.length == 0) {
            alert('Выберите раздел сайта(слева)');
            return false;
        }

        var siteSectionId = $selectedSiteSection.data('section-id');
        var apiSectionId = $(this).data('section-id');
        var apiSectionName = $(this).data('section-name');

        var matchedItem = new SectionMatchedItem(siteSectionId, apiSectionId, apiSectionName);

        if (matchCollection.append(matchedItem)) {
            var $parent = $selectedSiteSection.find('.section-mapper__matches:eq(0)');
            matchedItem.render($parent);
            $apiSectionWrap.addClass('disabled');

            matchedItem.onDelete = function($apiSectionWrap) {
                return function() {
                    $apiSectionWrap.removeClass('disabled');
                }
            }($apiSectionWrap);
        }

        return false;
    });

    /**
     * Проинциализируем дерево
     * Сохраненные соответствия из бд выводятся в дата-аттрибуты
     * Получим их оттуда и отрисуем
     */
    $('#section-mapper-site .section-mapper__section').each(function(){
        var $el = $(this);

        var matchInfo = $el.data('match-info');

        if (matchInfo != null) {
            var siteSectionId = $el.data('section-id');
            var $parent = $el.find('.section-mapper__matches:eq(0)');

            // Если для раздела есть какие-то соответствия развернем его и все родительские разделы
            $el.addClass('open');
            $el.parents('.section-mapper__section').addClass('open');

            matchInfo.forEach(function(match){
                var matchedItem = new SectionMatchedItem(siteSectionId, match.id, match.name, match.collapse);
                if (matchCollection.append(matchedItem)) {
                    matchedItem.render($parent);

                    var $matcher = $('.section-mapper__matcher[data-section-id="' + match.id + '"]');
                    var $apiSectionWrap = $matcher.closest('.section-mapper__section');
                    $apiSectionWrap.addClass('disabled');

                    matchedItem.onDelete = function($apiSectionWrap) {
                        return function() {
                            $apiSectionWrap.removeClass('disabled');
                        }
                    }($apiSectionWrap);
                }
            });
        }
    });

    /**
     * Кнопка "Свернуть все"
     */
    $('.js-section-mapper-toggle').click(function(){
        if ($(this).data('closed') === undefined) {
            var closed = false;
        } else {
            var closed = $(this).data('closed');
        }

        var $sections = $('#section-mapper-site .section-mapper__section');

        if (closed) {
            $sections.addClass('open');
        } else {
            $sections.removeClass('open');
        }


        $(this).data('closed', !closed);

        return false;
    });
});