<?php
namespace Ekf\Exchange\Service;

use Ekf\Exchange\Entity\ProductEntity;
use Exception;
use InvalidArgumentException;

/**
 * Ищет товары в инфоблоке, чтобы понять, существует ли такой товар или нет
 */
class ProductFinder
{
    private $bInited = false;

    private $productsCache = [];

    public function __construct()
    {
    }

    /**
     * @param $iblockId
     * @throws InvalidArgumentException
     */
    public function init($iblockId)
    {
        if ((int)$iblockId == 0) {
            throw new InvalidArgumentException(sprintf(
                'Ид инфоблока должен быть числом: "%s"', $iblockId
            ));
        }

        $this->fillExistingProductsMap($iblockId);

        $this->bInited = true;
    }

    /**
     * Получает
     *
     * @param ProductEntity $productEntity
     * @throws Exception
     * @return array
     */
    public function getExisting(ProductEntity $productEntity)
    {
        if (!$this->bInited) {
            throw new Exception('Сначал необходимо вызвать init()');
        }

        $existingProduct = $this->productsCache[$productEntity->getXmlId()];

        if (isset($existingProduct)) {
            return $existingProduct;
        }

        return [];
    }

    /**
     * Заполняет массив данными о существующих товарах
     * @param $iblockId
     */
    private function fillExistingProductsMap($iblockId)
    {
        $dbElements = \CIBlockElement::GetList(
            array(),
            array('IBLOCK_ID' => $iblockId, 'CHECK_PERMISSIONS' => 'N', 'PROPERTY_EKF_IS_EKF' => 1),
            false,
            false,
            array('ID', 'XML_ID', 'PROPERTY_EKF_HASH', 'PROPERTY_EKF_HASH_P', 'PROPERTY_EKF_HASH_F')
        );

        $this->productsCache = [];

        while ($arElement = $dbElements->Fetch()) {
            $this->productsCache[$arElement['XML_ID']] = $arElement;
        }
    }
}