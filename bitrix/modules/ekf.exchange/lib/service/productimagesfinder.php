<?php
namespace Ekf\Exchange\Service;

use Bitrix\Main\Loader;
use CFile;
use InvalidArgumentException;

class ProductImagesFinder
{
    private $productsCache = [];

    public function __construct($iblockId)
    {
        if ((int)$iblockId == 0) {
            throw new InvalidArgumentException(sprintf(
                'Ид инфоблока должен быть числом: "%s"', $iblockId
            ));
        }

        Loader::includeModule('iblock');

        $this->fillExistingProductsMap($iblockId);
    }

    /**
     * Заполняет массив данными о существующих товарах
     * Нам нужны названия файлов, т.к. в АПИ название файла - это его хэш
     * Сравнив название файла на сайте и в АПИ можно понять, изменился он или нет
     * @param $iblockId
     */
    private function fillExistingProductsMap($iblockId)
    {
        $dbElements = \CIBlockElement::GetList(
            array(),
            array('IBLOCK_ID' => $iblockId, 'CHECK_PERMISSIONS' => 'N', 'PROPERTY_EKF_IS_EKF' => 1),
            false,
            false,
            array('ID', 'XML_ID', 'PROPERTY_EKF_HASH_F', 'DETAIL_PICTURE')
        );

        $detailPicMap = [];

        while ($arElement = $dbElements->Fetch()) {
            $this->productsCache[$arElement['XML_ID']] = $arElement;

            if (!empty($arElement['DETAIL_PICTURE'])) {
                $detailPicMap[$arElement['DETAIL_PICTURE']] = $arElement['XML_ID'];
            }
        }

        $chunks = array_chunk($detailPicMap, 1000, true);

        foreach ($chunks as $chunk) {
            $filesIterator = CFile::GetList([], ['@ID' => array_keys($chunk)]);
            while ($file = $filesIterator->Fetch()) {
                $productXmlId = $detailPicMap[$file['ID']];
                $this->productsCache[$productXmlId]['DETAIL_PICTURE_EXTERNAL_ID'] = $file['EXTERNAL_ID'];
            }
        }
    }

    /**
     * @param $productXmlID
     * @return array
     */
    public function getExisting($productXmlID)
    {
        $existingProduct = $this->productsCache[$productXmlID];

        if (isset($existingProduct)) {
            return $existingProduct;
        }

        return [];
    }
}