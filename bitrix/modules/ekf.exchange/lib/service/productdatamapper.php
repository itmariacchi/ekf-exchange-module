<?php
namespace Ekf\Exchange\Service;

use Ekf\Exchange\Entity\ProductEntity;
use Ekf\Exchange\Entity\PropertyValueEntity;
use Ekf\Exchange\Exception\NoMatchingSectionException;
use Exception;

/**
 * Данные приходящие из АПИ неким образом отражаются на данные товаров сайта
 * Например категория А из АПИ должна быть импортирована в категорию Б
 * Свойство А - загружено в свойство Б
 */
class ProductDataMapper
{
    private $propertiesMap = [];

    /** @var SectionMapHelper  */
    private $sectionMapHelper;

    public function __construct()
    {
        $this->propertiesMap = (new Config())->getPropertiesMap();
        $this->sectionMapHelper = new SectionMapHelper();
    }

    public function withMappedData(ProductEntity $productEntity)
    {
        if (!empty($productEntity->getSectionId())) {
            $productEntity->setSectionId(
                $this->getMappedSection($productEntity->getSectionId())
            );
        }

        $newPropertyCollection = [];

        /** @var PropertyValueEntity $propertyValue */
        foreach ($productEntity->getPropertyCollection() as $propertyValue) {
            $code = $propertyValue->getPropertyCode() ?? $this->getMappedPropertyCode($propertyValue->getApiCode());
            if (!empty($code)) {
                $propertyValue->setPropertyCode($code);
                $newPropertyCollection[] = $propertyValue;
            }
        }

        $productEntity->setPropertyCollection($newPropertyCollection);

        return $productEntity;
    }

    /**
     * Находит для раздела товара из АПИ соответствующий раздел на сайте
     * @param $sectionXmlId
     * @return int
     * @throws Exception
     */
    public function getMappedSection($sectionXmlId)
    {
        $sectionId = $this->sectionMapHelper->getMatchedSectionForProduct($sectionXmlId);
        if ((int)$sectionId == 0) {
            throw new NoMatchingSectionException();
        }

        return $sectionId;
    }

    /**
     * Находит для кода свойства из АПИ ид свойства на сайте, куда оно должно быть загружено
     * @param $code
     * @return string
     */
    public function getMappedPropertyCode($code)
    {
        return !empty($this->propertiesMap[$code]) ? $this->propertiesMap[$code] : '';
    }
}