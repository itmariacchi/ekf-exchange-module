<?php
namespace Ekf\Exchange\Factory;

use Ekf\Exchange\Entity\PropertyValueEntity;
use Ekf\Exchange\Exception\BadPropertyDataException;
use InvalidArgumentException;

/**
 * Фабрика для объектов PropertyValueEntity
 */
class PropertyValueFactory
{
    /**
     * @param $valueData array
     * @return PropertyValueEntity
     * @throws BadPropertyDataException
     */
    public function buildFromArray($valueData)
    {
        if (!is_array($valueData) || empty($valueData)) {
            throw new InvalidArgumentException('Данные о значении свойства должны быть непустым массивом');
        }

        if (!array_key_exists('value', $valueData)) {
            throw new InvalidArgumentException('Для значения свойства должно быть задано поле value');
        }

        $apiCode = trim($valueData['api_code']);

        $code = trim($valueData['code']);

        if (empty($apiCode) && empty($code)) {
            throw new BadPropertyDataException('Для значения свойства должен быть заполнен либо апи код (api_code), либо код на сайте (code)');
        }

        $valueEntity = new PropertyValueEntity();

        if (!empty($apiCode)) {
            $valueEntity->setApiCode($apiCode);
        }

        if (!empty($code)) {
            $valueEntity->setPropertyCode($code);
        }

        if (!empty($valueData['name'])) {
            $valueEntity->setName($valueData['name']);
        }

        if (!empty($valueData['unit'])) {
            $valueEntity->setUnit($valueData['unit']);
        }

        if (!empty($valueData['etim_unit'])) {
            $valueEntity->setEtimUnit($valueData['etim_unit']);
        }

        $valueEntity->setValue($valueData['value']);

        return $valueEntity;
    }
}