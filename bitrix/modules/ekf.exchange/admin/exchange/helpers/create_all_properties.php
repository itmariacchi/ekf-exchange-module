<?
/**
 * Создает все свойства из АПИ и сохраняет маппинг
 */

use Ekf\Exchange\Service\ApiClient;
use Ekf\Exchange\Service\Config;

require(__DIR__ . '/../init.php');

$configManager = new Config();

$apiClient = new ApiClient($configManager->getApiKey());

$iblockId = $configManager->getIblockId();

if ($iblockId == 0) {
    exit();
}

$propertiesMap = $configManager->getPropertiesMap();

$apiProperties = $apiClient->getProperties();

$propertyManager = new CIBlockProperty();

foreach ($apiProperties as $apiProperty) {
    if (array_key_exists($apiProperty['id'], $propertiesMap)) {
        continue;
    }

    $sitePropertyCode = CUtil::translit($apiProperty['name'], 'ru', [
        'replace_space' => '_',
        'replace_other' => '_',
        'change_case'   => 'U'
    ]);

    $propertyData = [
        'NAME' => $apiProperty['name'],        
        'XML_ID' => $apiProperty['id'],
        'CODE' => $sitePropertyCode,
        'PROPERTY_TYPE' => 'S',
        'IBLOCK_ID' => $iblockId
    ];

    $existingProperty = CIBlockProperty::GetList([], ['IBLOCK_ID' => $iblockId, 'XML_ID' => $propertyData['XML_ID']])->Fetch();
    if ($existingProperty) {
        continue;
    }

    $propertyId = $propertyManager->Add($propertyData);
    if (!$propertyId) {
//        echo 'Ошибка создания свойства: ' . $propertyManager->LAST_ERROR;
//        echo '<pre>';print_r($propertyData);echo '</pre>';
//        exit();
        continue;
    }

    $propertiesMap[$apiProperty['id']] = $sitePropertyCode;
}

$configManager->savePropertiesMap($propertiesMap);

echo 'done';
