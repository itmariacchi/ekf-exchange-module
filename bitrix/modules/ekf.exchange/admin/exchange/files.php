<?php
use Ekf\Exchange\Service\DataDownloader;

require(__DIR__ . '/init.php');

try {
    $logger = new \Ekf\Exchange\Service\Logger('files');

    $logger->state('Начат обмен файлами');

    if (empty($configManager->getMorePhotoPropertyCode())) {
        throw new Exception('В настройках не указано свойство для загрузки доп. изображений');
    }

    $imagesFinder = new \Ekf\Exchange\Service\ProductImagesFinder($iblockId);

    $imagesUpdater = new \Ekf\Exchange\Service\ProductFilesUpdater();

    $queueRepository = new \Ekf\Exchange\Repository\QueueRepository();

    $totalRecords = (new DataDownloader)->loadDataToQueue(
        [$apiClient, 'getFiles'], false, 'file'
    );

    $objElement = new CIBlockElement();

    $totalProductsProcessed = 0;

    $connection = \Bitrix\Main\Application::getConnection();

    do {
        $totalRecords = $queueRepository->getQueueSize();

        if ($totalProductsProcessed % 500 == 0) {
            $connection->disconnect();
            $connection->connect();
        }

        $records = $queueRepository->getFromQueue(100);

        foreach ($records as $record) {

            try {
                $singleProductFiles = unserialize($record['data']);

                if (!is_array($singleProductFiles)) {
                    throw new Exception(sprintf('Ошибка десериализации: %s', $record['data']));
                }

                $existingProduct = $imagesFinder->getExisting($singleProductFiles['id']);

                //echo $singleProductFiles['id'] . ' / ' . ($totalRecords--) . PHP_EOL;
                ob_end_flush();

                if (!$existingProduct) {
                    continue;
                }

                $filesByType = [];

                foreach ($singleProductFiles['files'] as $file) {
                    $typeCode = $file['fileType']['code'];
                    $file['+base_name'] = basename($file['file']);
                    $filesByType[$typeCode][] = $file;
                }

                $filesByType['main_image'] = $filesByType['main_image'][0];

                // хэш доп. изображений
                $additionalImagesHash = md5(implode('|', array_column($filesByType['additional_image'], '+base_name')));

                // Доп. изображения изменились, перезальем
                if ($forceUpdate || $additionalImagesHash != $existingProduct['PROPERTY_EKF_HASH_F_VALUE']) {
                    $imagesUpdater->update(
                        $existingProduct['ID'],
                        $iblockId,
                        $configManager->getMorePhotoPropertyCode(),
                        $filesByType['additional_image']);

                        \CIBlockElement::SetPropertyValuesEx($existingProduct['ID'], $iblockId, ['EKF_HASH_F' => $additionalImagesHash]);

                    $logger->info(sprintf(
                        'Для товара %s [%d] обновлены дополнительные изображения', $singleProductFiles['id'], $existingProduct['ID']
                    ));
                }

                // Основное изображение изменилось, перезальем
                if ($forceUpdate || $existingProduct['DETAIL_PICTURE_EXTERNAL_ID'] != $filesByType['main_image']['+base_name']) {
                    $detailPicture = CFile::MakeFileArray($filesByType['main_image']['file']);

                    if (!is_array($detailPicture) || empty($detailPicture) || $detailPicture['type'] == 'unknown') {
                        throw new Exception(sprintf('Невозможно получить изображение %s', $filesByType['main_image']['file']));
                    }

                    $detailPicture['external_id'] = $filesByType['main_image']['+base_name'];

                    $productData = [
                        'ID'             => $existingProduct['ID'],
                        'IBLOCK_ID'      => $iblockId,
                        'DETAIL_PICTURE' => $detailPicture,
                        'skip_ekf_hash_reset' => true
                    ];

                    $bSuccess = $objElement->Update($existingProduct['ID'], $productData, false, false, false, false);

                    unlink($detailPicture['tmp_name']);

                    if (!$bSuccess) {
                        throw new Exception(sprintf(
                            'При обновлении товара %s [%d] произошла ошибка: ', $singleProductFiles['id'], $existingProduct['ID'], $objElement->LAST_ERROR
                        ));
                    }

                    $logger->info(sprintf(
                        'Для товара %s [%d] обновлено основное изображение', $singleProductFiles['id'], $existingProduct['ID']
                    ));
                }

                $totalProductsProcessed++;

            } catch (Exception $ex) {
                $logger->critical($logger->formatException($ex), [
                    'product_id' => $singleProductFiles['id'],
                    'trace' => $ex->getTraceAsString()
                ]);
            }
        }

        //TODO::remove
        //break;
    } while (count($records) > 0);

    echo 'TOTAL: ' . $totalProductsProcessed . PHP_EOL;

} catch (Exception $ex) {
    echo $ex->getMessage(); // На случай если сам логгер выбросил исключение

    $logger->critical($logger->formatException($ex), [
        'trace' => $ex->getTraceAsString()
    ]);
}

$logger->state('Завершен обмен файлами');